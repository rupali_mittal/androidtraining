package com.hp.androidassignment

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RatingBar
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_example.*

class ExampleFragment : Fragment(), RatingBar.OnRatingBarChangeListener {


    private lateinit var rootView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_example, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootView = view;
        ratingBar.onRatingBarChangeListener = this


    }

    override fun onResume() {
        super.onResume()
        rgReview.setOnCheckedChangeListener { group, checkedId ->
            val radio: RadioButton = rootView.findViewById(checkedId)
            changeOrientation()
            if (radio == rbYes) {
                tvArticle.text = getString(R.string.article_like)
                setVisibility(View.VISIBLE)
            } else {
                tvArticle.text = getString(R.string.article_thanks)
                setVisibility(View.INVISIBLE)
            }
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    fun changeOrientation() {
        if (activity?.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        else {
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

    fun setVisibility(visibility: Int) {
        tvSong.visibility = visibility
        backgroundView.visibility = visibility
        ratingBar.visibility = visibility
    }

    override fun onRatingChanged(p0: RatingBar?, p1: Float, p2: Boolean) {
        (activity as MainActivity).showToast(p1);
    }
}

